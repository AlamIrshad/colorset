// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import TouchControl from "./TouchControl";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    box:cc.Prefab = null;
    @property(cc.Node)
    emptyNodeForBox:cc.Node = null;
    @property(cc.JsonAsset)
    ColorPatten:cc.JsonAsset = null;
    @property(cc.JsonAsset)
    hardlevels:cc.JsonAsset = null;
    @property(cc.ProgressBar)
    progressbar: cc.ProgressBar | null = null;
    @property(cc.Node)
    PauseMenuSprite:cc.Node = null;
    @property(cc.Node)
    ResumebtnNode:cc.Node = null;
    @property(cc.Node)
    GameOverLabel:cc.Node = null;
    @property({type:TouchControl})
    TouchCtrl: TouchControl = null;
    counter = 0;
    colorPaternindex1;
    colorPaternindex2;
    colorPaternindex3;
    colorPaternindex4;
    colorPaternindex5;
    colorPaternindex6;
    posY =70;
    posX =-97;
    colorR;
    colorG;
    colorB;
    colorR1;
    colorG1;
    colorB1;
    colorR2;
    colorG2;
    colorB2;
    colorR3; colorR4; colorR5; colorR6; colorR7;
    colorG3; colorG4; colorG5; colorG6; colorG7;
    colorB3; colorB4; colorB5; colorB6; colorB7;
    tempvar;
    timer:number=0.1;
    largestColorNo:number;
    numberOfCubes:number;
    counter_for_difficulty_groups = 0;
    LifeLine_1;
    No_Of_Boxes;

    diffcultyIndex;
    diffcultyIndexNmber;
    NoOfDifficultyQuestions;
    NoOfDifficultyQuestionslength;
    
    onLoad(){
        this.PauseMenuSprite.active = false;
        this.GameOverLabel.active = false;
    }
    start () {
        this.LifeLine_1 = this.ColorPatten.json[0]["Lives"][0];
        this.diffcultyIndexNmber = 1;
        this.NoOfDifficultyQuestions = 0;
        this.createBox();
    }

    createBox()                                                                                //Box Creation
    {   this.posY =70;
        this.posX =-97;
        this.diffcultyIndex = this.hardlevels.json[0]['Difficulty'][this.diffcultyIndexNmber][0];
        this.NoOfDifficultyQuestionslength = this.hardlevels.json[0]['Difficulty'][this.diffcultyIndexNmber][1];
        
        //console.info(ps);
        var ColourArrayLength =  this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions].length;
        this.colorPaternindex1 = this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions][2];
        this.colorPaternindex2 = this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions][3];
        this.colorPaternindex3 = this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions][4];
        this.colorPaternindex4 = this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions][5];
        this.colorPaternindex5 = this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions][6];
        this.colorPaternindex6 = this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions][7];

        this.No_Of_Boxes = this.ColorPatten.json[0][this.diffcultyIndex][this.NoOfDifficultyQuestions][1];
        if(this.NoOfDifficultyQuestions<this.NoOfDifficultyQuestionslength)
        {
            this.NoOfDifficultyQuestions++;
            if(this.NoOfDifficultyQuestions == this.NoOfDifficultyQuestionslength)
            {   this.NoOfDifficultyQuestions = 0;
                this.diffcultyIndexNmber++;
            }
        }

        console.info(this.diffcultyIndexNmber + '         '+ this.NoOfDifficultyQuestions)
        this.numberOfCubes = this.No_Of_Boxes*this.No_Of_Boxes;
        if(this.No_Of_Boxes == 3)
        {
            this.posY =  70;
            this.posX = -75;
            var XgapOfBoxes = 80;
            var YgapOfBoxes = 80;
            var temposx = this.posX;
        }
        if(this.No_Of_Boxes == 4)
        {
            this.posY =  100;
            this.posX = -99;
            var XgapOfBoxes = 64;
            var YgapOfBoxes = 64;
            var temposx = this.posX;
        }
        if(this.No_Of_Boxes ==5)
        {
            this.posY =  100;
            this.posX = -110;
            var XgapOfBoxes = 55;
            var YgapOfBoxes = 55;
            var temposx = this.posX;
        }
        if(this.No_Of_Boxes ==6)
        {
            this.posY =  100;
            this.posX = -123;
            var XgapOfBoxes = 48;
            var YgapOfBoxes = 48;
            var temposx = this.posX;
        }

        var colorRandom = new Set();
        do{
            colorRandom.add(Math.floor(7*Math.random()));
        }
        while(colorRandom.size<6);
        var Iterator = colorRandom.values();
        var arr = new Array(4);
        for(var i=0; i<6; i++)
        {
            arr[i] = Iterator.next().value;
        }
        this.colorR = this.ColorPatten.json[0]["ColourCodes"][arr[0]][0];
        this.colorG =this.ColorPatten.json[0]["ColourCodes"][arr[0]][1];
        this.colorB =this.ColorPatten.json[0]["ColourCodes"][arr[0]][2];

        this.colorR1 =this.ColorPatten.json[0]["ColourCodes"][arr[1]][0];
        this.colorG1 =this.ColorPatten.json[0]["ColourCodes"][arr[1]][1];
        this.colorB1 =this.ColorPatten.json[0]["ColourCodes"][arr[1]][2];

        this.colorR2 = this.ColorPatten.json[0]["ColourCodes"][arr[2]][0];
        this.colorG2 =this.ColorPatten.json[0]["ColourCodes"][arr[2]][1];
        this.colorB2 =this.ColorPatten.json[0]["ColourCodes"][arr[2]][2];

        this.colorR3 = this.ColorPatten.json[0]["ColourCodes"][arr[3]][0];
        this.colorG3 =this.ColorPatten.json[0]["ColourCodes"][arr[3]][1];
        this.colorB3 =this.ColorPatten.json[0]["ColourCodes"][arr[3]][2];

        this.colorR4 = this.ColorPatten.json[0]["ColourCodes"][arr[4]][0];
        this.colorG4 =this.ColorPatten.json[0]["ColourCodes"][arr[4]][1];
        this.colorB4 =this.ColorPatten.json[0]["ColourCodes"][arr[4]][2];

        this.colorR5 = this.ColorPatten.json[0]["ColourCodes"][arr[5]][0];
        this.colorG5 =this.ColorPatten.json[0]["ColourCodes"][arr[5]][1];
        this.colorB5 =this.ColorPatten.json[0]["ColourCodes"][arr[5]][2];
        // this.colorR6 = this.ColorPatten.json[0]["ColourCodes"][arr[6]][0];
        // this.colorG6 =this.ColorPatten.json[0]["ColourCodes"][arr[6]][1];
        // this.colorB6 =this.ColorPatten.json[0]["ColourCodes"][arr[6]][2];
        // // this.colorR7 = this.ColorPatten.json[0]["ColourCodes"][arr[7]][0];
        // // this.colorG7 =this.ColorPatten.json[0]["ColourCodes"][arr[7]][1];
        // // this.colorB7 =this.ColorPatten.json[0]["ColourCodes"][arr[7]][2]


        for(var i=1; i<=this.numberOfCubes; i++)
        {   
            var BoxNode = cc.instantiate(this.box);
            if(this.No_Of_Boxes==3)
            {
                BoxNode.setContentSize(80,80);
                BoxNode.children[0].setContentSize(75,75); 
            }
            if(this.No_Of_Boxes == 4)
            {
                BoxNode.setContentSize(64,64);
                BoxNode.children[0].setContentSize(60,60);
            }
            if(this.No_Of_Boxes == 5)
            {
                BoxNode.setContentSize(55,55);
                BoxNode.children[0].setContentSize(52,52);
            }
            if(this.No_Of_Boxes == 6)
            {
                BoxNode.setContentSize(48,48);
                BoxNode.children[0].setContentSize(45,45);
            }
            BoxNode.setPosition(this.posX,this.posY);
            this.emptyNodeForBox.addChild(BoxNode);
            this.posX +=XgapOfBoxes;
            this.counter++;
            if(this.counter==this.No_Of_Boxes)
            {   this.counter = 0;
                this.posY =  this.posY-YgapOfBoxes;
                this.posX = temposx
            }  
            
        }
        for(var i=0; i<this.numberOfCubes;i++)
        {
         this.emptyNodeForBox.children[i].getComponent(cc.Animation).play();
        }

        if(ColourArrayLength==4)
        {
            this.fillTwoColor();
        }
        if(ColourArrayLength ==5)
        {
            this.fillThreeColor();
        }
        if(ColourArrayLength == 6)
        {
            this.fillfourColor();
        }
        if(ColourArrayLength == 7)
        {
            this.fillfiveColor()
        }
        if(ColourArrayLength == 8)
        {
            this.fillSixColor()
        }
       this.TouchCtrl.TouchActive(this.emptyNodeForBox, this.largestColorNo); 
    }
   
    fillTwoColor()
    {   
        for(var i=0; i<this.colorPaternindex1.length; i++)
        {
            var t = this.colorPaternindex1[i];
            this.largestColorNo = t;
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR,this.colorG,this.colorB,255);
        }
        for(var i=0; i<this.colorPaternindex2.length; i++)
        {
            var t = this.colorPaternindex2[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR1,this.colorG1,this.colorB1,255);
        }
    }
    fillThreeColor(){
        for(var i=0; i<this.colorPaternindex1.length; i++)
        {
            var t = this.colorPaternindex1[i];
            this.largestColorNo = t;
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR,this.colorG,this.colorB,255);
        }
        for(var i=0; i<this.colorPaternindex2.length; i++)
        {
            var t = this.colorPaternindex2[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR1,this.colorG1,this.colorB1,255);
        }
        for(var i=0; i<this.colorPaternindex3.length; i++)
        {
            var t = this.colorPaternindex3[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR2,this.colorG2,this.colorB2,255);
        }

    }
    fillfourColor(){
        for(var i=0; i<this.colorPaternindex1.length; i++)
        {
            var t = this.colorPaternindex1[i];
            this.largestColorNo = t;
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR,this.colorG,this.colorB,255);
        }
        for(var i=0; i<this.colorPaternindex2.length; i++)
        {
            var t = this.colorPaternindex2[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR1,this.colorG1,this.colorB1,255);
        }
        for(var i=0; i<this.colorPaternindex3.length; i++)
        {
            var t = this.colorPaternindex3[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR2,this.colorG2,this.colorB2,255);
        }
        for(var i=0; i<this.colorPaternindex4.length; i++)
        {
            var t = this.colorPaternindex4[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR3,this.colorG3,this.colorB3,255);
        }
    }
    
    fillfiveColor(){
        for(var i=0; i<this.colorPaternindex1.length; i++)
        {
            var t = this.colorPaternindex1[i];
            this.largestColorNo = t;
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR,this.colorG,this.colorB,255);
        }
        for(var i=0; i<this.colorPaternindex2.length; i++)
        {
            var t = this.colorPaternindex2[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR1,this.colorG1,this.colorB1,255);
        }
        for(var i=0; i<this.colorPaternindex3.length; i++)
        {
            var t = this.colorPaternindex3[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR2,this.colorG2,this.colorB2,255);
        }
        for(var i=0; i<this.colorPaternindex4.length; i++)
        {
            var t = this.colorPaternindex4[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR3,this.colorG3,this.colorB3,255);
        }
        for(var i=0; i<this.colorPaternindex5.length; i++)
        {
            var t = this.colorPaternindex5[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR4,this.colorG4,this.colorB4,255);
        }
    }

    fillSixColor(){
        console.info('this active');
        for(var i=0; i<this.colorPaternindex1.length; i++)
        {
            var t = this.colorPaternindex1[i];
            this.largestColorNo = t;
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR,this.colorG,this.colorB,255);
        }
        for(var i=0; i<this.colorPaternindex2.length; i++)
        {
            var t = this.colorPaternindex2[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR1,this.colorG1,this.colorB1,255);
        }
        for(var i=0; i<this.colorPaternindex3.length; i++)
        {
            var t = this.colorPaternindex3[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR2,this.colorG2,this.colorB2,255);
        }
        for(var i=0; i<this.colorPaternindex4.length; i++)
        {
            var t = this.colorPaternindex4[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR3,this.colorG3,this.colorB3,255);
        }
        for(var i=0; i<this.colorPaternindex5.length; i++)
        {
            var t = this.colorPaternindex5[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR4,this.colorG4,this.colorB4,255);
        }
        for(var i=0; i<this.colorPaternindex6.length; i++)
        {
            var t = this.colorPaternindex6[i];
            this.emptyNodeForBox.children[t-1].children[0].color = cc.color(this.colorR5,this.colorG5,this.colorB5,255);
        }
    }

    
    update (dt) {
       
        this.timer-= dt;
        if (this.timer <= 0) {
            this.progressbar.progress+=0.0018;
            this.timer =0.1;
        }
        if(this.progressbar.progress>=1)
        {
            this.ResumebtnNode.active = false;
            this.PauseMenuSprite.active = true;
            this.GameOverLabel.active = true;
           
        }
        
    }

    onPauseButtonClicked(){
        this.PauseMenuSprite.active = true;
        this.PauseMenuSprite.getComponent(cc.Animation).play('PauseMenuSlideAnim');
    }
    onResumeButtonClicked(){
        this.PauseMenuSprite.getComponent(cc.Animation).play('PauseMenuSlideAnim2');
    }
    onRestartButtonClicked(){
        cc.director.loadScene('mainScene');
    }
    
}
