// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import BoxGeneration from "./BoxGeneration";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

  @property({type:BoxGeneration})
  BoxGenerate: BoxGeneration = null;
  @property(cc.Prefab)
  LifeLineSymbol:cc.Prefab = null;
  @property(cc.Label)
  scoreBox:cc.Label= null;
  @property(cc.Node)
  Right:cc.Node = null;
  @property(cc.Node)
  Wrong:cc.Node = null;
  @property(cc.Node)
  Downsidebar:cc.Node = null;
  @property(cc.AudioClip)
  rightclicksound: cc.AudioClip = null;
  @property(cc.AudioClip)
  wrongclicksound: cc.AudioClip = null;
  EmptyNode;
  largestColorNo;
  scoreNumber=0;
  lifelineindicator = new Array(6);
  LifeLine:number;
  audioComp
    // LIFE-CYCLE CALLBACKS:

     onLoad () {
        this.scoreNumber=0;
        this.Right.active = false;
        this.Wrong.active = false;
        this.Downsidebar.active = false;
    }

    start () {
        this.audioComp = this.getComponent(cc.AudioSource)
        this.LifeLine = this.BoxGenerate.LifeLine_1;
        var X_positionOfLifelineSymbol = -120;
        for(var i=0;i<this.lifelineindicator.length; i++)
        {
            this.lifelineindicator[i] = cc.instantiate(this.LifeLineSymbol);
            this.lifelineindicator[i].parent = this.node.parent;
            this.lifelineindicator[i].setPosition(X_positionOfLifelineSymbol,309);
            X_positionOfLifelineSymbol +=22;
        }
    }
    TouchActive(EmtyNod:cc.Node, largestColor:Number)
    {
        this.EmptyNode = EmtyNod;
        this.largestColorNo = largestColor;
        this.EmptyNode.children[0].on(cc.Node.EventType.TOUCH_START,this.onTouchSt0,this,true);
        this.EmptyNode.children[1].on(cc.Node.EventType.TOUCH_START,this.onTouchSt1,this,true);
        this.EmptyNode.children[2].on(cc.Node.EventType.TOUCH_START,this.onTouchSt2,this,true);
        this.EmptyNode.children[3].on(cc.Node.EventType.TOUCH_START,this.onTouchSt3,this,true);
        this.EmptyNode.children[4].on(cc.Node.EventType.TOUCH_START,this.onTouchSt4,this,true);
        this.EmptyNode.children[5].on(cc.Node.EventType.TOUCH_START,this.onTouchSt5,this,true);
        this.EmptyNode.children[6].on(cc.Node.EventType.TOUCH_START,this.onTouchSt6,this,true);
        this.EmptyNode.children[7].on(cc.Node.EventType.TOUCH_START,this.onTouchSt7,this,true);
        this.EmptyNode.children[8].on(cc.Node.EventType.TOUCH_START,this.onTouchSt8,this,true);
        if(this.BoxGenerate.No_Of_Boxes>3){
        this.EmptyNode.children[9].on(cc.Node.EventType.TOUCH_START,this.onTouchSt9,this,true);
        this.EmptyNode.children[10].on(cc.Node.EventType.TOUCH_START,this.onTouchSt10,this,true);
        this.EmptyNode.children[11].on(cc.Node.EventType.TOUCH_START,this.onTouchSt11,this,true);
        this.EmptyNode.children[12].on(cc.Node.EventType.TOUCH_START,this.onTouchSt12,this,true);
        this.EmptyNode.children[13].on(cc.Node.EventType.TOUCH_START,this.onTouchSt13,this,true);
        this.EmptyNode.children[14].on(cc.Node.EventType.TOUCH_START,this.onTouchSt14,this,true);
        this.EmptyNode.children[15].on(cc.Node.EventType.TOUCH_START,this.onTouchSt15,this,true);
        }
        if(this.BoxGenerate.No_Of_Boxes>4)
        {
        this.EmptyNode.children[16].on(cc.Node.EventType.TOUCH_START,this.onTouchSt16,this,true);
        this.EmptyNode.children[17].on(cc.Node.EventType.TOUCH_START,this.onTouchSt17,this,true);
        this.EmptyNode.children[18].on(cc.Node.EventType.TOUCH_START,this.onTouchSt18,this,true);
        this.EmptyNode.children[19].on(cc.Node.EventType.TOUCH_START,this.onTouchSt19,this,true);
        this.EmptyNode.children[20].on(cc.Node.EventType.TOUCH_START,this.onTouchSt20,this,true);
        this.EmptyNode.children[21].on(cc.Node.EventType.TOUCH_START,this.onTouchSt21,this,true);
        this.EmptyNode.children[22].on(cc.Node.EventType.TOUCH_START,this.onTouchSt22,this,true);
        this.EmptyNode.children[23].on(cc.Node.EventType.TOUCH_START,this.onTouchSt23,this,true);
        this.EmptyNode.children[24].on(cc.Node.EventType.TOUCH_START,this.onTouchSt24,this,true);
        }
        if(this.BoxGenerate.No_Of_Boxes>5)
        {
        this.EmptyNode.children[25].on(cc.Node.EventType.TOUCH_START,this.onTouchSt25,this,true);
        this.EmptyNode.children[26].on(cc.Node.EventType.TOUCH_START,this.onTouchSt26,this,true);
        this.EmptyNode.children[27].on(cc.Node.EventType.TOUCH_START,this.onTouchSt27,this,true);
        this.EmptyNode.children[28].on(cc.Node.EventType.TOUCH_START,this.onTouchSt28,this,true);
        this.EmptyNode.children[29].on(cc.Node.EventType.TOUCH_START,this.onTouchSt29,this,true);
        this.EmptyNode.children[30].on(cc.Node.EventType.TOUCH_START,this.onTouchSt30,this,true);
        this.EmptyNode.children[31].on(cc.Node.EventType.TOUCH_START,this.onTouchSt31,this,true);
        this.EmptyNode.children[32].on(cc.Node.EventType.TOUCH_START,this.onTouchSt32,this,true);
        this.EmptyNode.children[33].on(cc.Node.EventType.TOUCH_START,this.onTouchSt33,this,true);
        this.EmptyNode.children[34].on(cc.Node.EventType.TOUCH_START,this.onTouchSt34,this,true);
        this.EmptyNode.children[35].on(cc.Node.EventType.TOUCH_START,this.onTouchSt35,this,true);
        }
    }

    onTouchSt0(){
        if(this.EmptyNode.children[0].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {   this.scoreNumber = this.scoreNumber+100;    console.info(this.largestColorNo);
            this.Right.active = true;
            this.forDestroyandRecreateBox();
            cc.audioEngine.play(this.rightclicksound,false,1);
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    
    onTouchSt1(){
        if(this.EmptyNode.children[1].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {   cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt2(){
        if(this.EmptyNode.children[2].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt3(){
        if(this.EmptyNode.children[3].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
            cc.audioEngine.play(this.rightclicksound,false,1);
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt4(){
        if(this.EmptyNode.children[4].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt5(){
        if(this.EmptyNode.children[5].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt6(){
        if(this.EmptyNode.children[6].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt7(){
        if(this.EmptyNode.children[7].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt8(){
        if(this.EmptyNode.children[8].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt9(){
        if(this.EmptyNode.children[9].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt10(){
        if(this.EmptyNode.children[10].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt11(){
        if(this.EmptyNode.children[11].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt12(){
        if(this.EmptyNode.children[12].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt13(){
        if(this.EmptyNode.children[13].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt14(){
        if(this.EmptyNode.children[14].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt15(){
        if(this.EmptyNode.children[15].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else {this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
    }
    onTouchSt16(){
        if(this.EmptyNode.children[16].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100; 
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt17(){
        if(this.EmptyNode.children[17].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100; 
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt18(){
        if(this.EmptyNode.children[18].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt19(){
        if(this.EmptyNode.children[19].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt20(){
        if(this.EmptyNode.children[20].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt21(){
        if(this.EmptyNode.children[21].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100; 
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt22(){
        if(this.EmptyNode.children[22].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt23(){
        if(this.EmptyNode.children[23].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;    
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt24(){
        if(this.EmptyNode.children[24].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt25(){
        if(this.EmptyNode.children[25].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt26(){
        if(this.EmptyNode.children[26].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt27(){
        if(this.EmptyNode.children[27].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt28(){
        if(this.EmptyNode.children[28].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt29(){
        if(this.EmptyNode.children[29].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt30(){
        if(this.EmptyNode.children[30].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt31(){
        if(this.EmptyNode.children[31].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt32(){
        if(this.EmptyNode.children[32].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt33(){
        if(this.EmptyNode.children[33].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt34(){
        if(this.EmptyNode.children[34].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }
    onTouchSt35(){
        if(this.EmptyNode.children[35].children[0].color.toString()== this.EmptyNode.children[this.largestColorNo-1].children[0].color.toString())
        {    cc.audioEngine.play(this.rightclicksound,false,1);
            this.scoreNumber = this.scoreNumber+100;
            this.Right.active = true;
            this.forDestroyandRecreateBox();
        }
        else 
        {
            this.Wrong.active = true;
            this.Downsidebar.active = true;
            cc.audioEngine.play(this.wrongclicksound,false,1);
            this.forDestroyandRecreateBox();
            this.LifeLine--;
            this.lifelineindicator[this.LifeLine].color = cc.color(170,170,170,255);
        }
       
    }




    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    forDestroyandRecreateBox(){

        for(var i=0; i<this.EmptyNode.children.length;i++)
        {
         this.EmptyNode.children[i].children[0].color = cc.color(150,150,150,255);
        }
        this.scheduleOnce(this.destroybox,0.1);
    }
    destroybox(){
        this.Right.active = false;
        this.Wrong.active = false;
        this.Downsidebar.active = false;
        this.EmptyNode.destroyAllChildren();
        this.scheduleOnce(this.createboxagain,0.08);
    }
    createboxagain(){
        this.BoxGenerate.createBox();
    }
    cont =0;
    update (dt) {
        if(this.LifeLine==0)
        {
            this.BoxGenerate.ResumebtnNode.active = false;
            this.BoxGenerate.PauseMenuSprite.active = true;
            this.BoxGenerate.GameOverLabel.active = true;
            this.BoxGenerate.PauseMenuSprite.getComponent(cc.Animation).play('PauseMenuSlideAnim');
            this.LifeLine--;
        }
        if(this.cont<this.scoreNumber)
        {   this.cont += 5;
            this.scoreBox.string =''+this.cont;
            if(this.cont==this.scoreNumber)
            {
                this.cont = this.scoreNumber;
            }
        }
        
     }
}
